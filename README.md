# mcl_ore_crops

## Description
This is a minetest mod meant for the mineclone2 subgame which adds unrealistic plants that let you grow minerals such as iron and gold

## Added crops
- Iron
- Gold

## Future crops
- Emerald - should grow on trees (because its money)
- Redstone - maybe a pumpkin like plan that grows redstone blocks
- Diamond - should take forever to grow

## Usage
To plant iron crops rightclick an iron nugget on farmland. You will be able to tell it is fully grown when the leaves sag downward

Likewise, gold crops can be planted by rightclicking a gold nugget on farmland. You will be able to tell it is fully grown when there are three leaves

## Credits
TheRandomLegoBrick - for creating plant textures and writing this mod

PilzAdam - for creating the farming mod that mcl2 uses

Textures are liscensed under CC0 see `liscense.txt` located in the `textures` folder
or https://creativecommons.org/publicdomain/zero/1.0/legalcode
code is liscensed under GPLv3, see `lisense.txt` located in the root directory of this mod
or https://www.gnu.org/licenses/gpl-3.0.en.html
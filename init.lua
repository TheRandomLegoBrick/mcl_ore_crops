--
-- mcl_ore_crops
--
-- Created by RandomLegoBrick
--
-- Credit to PilzAdam for creating the farming mod
-- 

-- Iron plants --
dofile(minetest.get_modpath("mcl_ore_crops").."/iron.lua")

-- Gold Plants --
dofile(minetest.get_modpath("mcl_ore_crops").."/gold.lua")

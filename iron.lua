local S = minetest.get_translator(minetest.get_current_modname())

minetest.override_item("mcl_core:iron_nugget", {
	on_place = function(itemstack, placer, pointed_thing)
		return mcl_farming:place_seed(itemstack, placer, pointed_thing, "mcl_ore_crops:iron_1")
	end
})

local selection_box_height = {
	-3/16,
	-2/16, -2/16,
	2/16, 2/16,
	6/16, 6/16,
}

for i=1,7 do
	local create, name, longdesc, stage

	if i == 1 then
		create = true
		name = S("Premature Iron Plant")
		longdesc = S([[
			Premature iron plants grow on farmland under sunlight in 5 stages.
			On hydrated farmland, they grow faster. They can be harvested at any time but will only yield a profit when mature.
		]])
	else
		create = false
	end

	if i == 1 then
		stage = 0
	elseif i <= 3 then
		stage = 1
	elseif i <= 5 then
		stage = 2
	else
		stage = 3
	end

	minetest.register_node("mcl_ore_crops:iron_"..i, {
		description = S("Premature Iron Plant (Stage @1)", i),
		_doc_items_create_entry = create,
		_doc_items_entry_name = name,
		_doc_items_longdesc = longdesc,
		paramtype = "light",
		sunlight_propagates = true,
		walkable = false,
		drawtype = "plantlike",
		drop = "mcl_core:iron_nugget",
		tiles = {"mcl_ore_crops_iron_stage_"..(stage)..".png"},
		inventory_image = "mcl_ore_crops_iron_stage_"..(stage)..".png",
		wield_image = "mcl_ore_crops_iron_stage_"..(stage)..".png",
		selection_box = {
			type = "fixed",
			fixed = {
				{-0.5, -0.5, -0.5, 0.5, selection_box_height[i], 0.5}
			},
		},
		groups = {dig_immediate=3, not_in_creative_inventory=1, plant=1, attached_node=1,
			dig_by_water=1, destroy_by_lava_flow=1, dig_by_piston=1},
		sounds = mcl_sounds.node_sound_leaves_defaults(),
		_mcl_blast_resistance = 0,
	})
end

minetest.register_node("mcl_ore_crops:iron", {
	description = S("Mature Iron Plant"),
	_doc_items_longdesc = S([[
		Mature iron plants are ready to be harvested for iron ingots.
		They won't grow any further.
	]]),
	sunlight_propagates = true,
	paramtype = "light",
	walkable = false,
	drawtype = "plantlike",
	tiles = {"mcl_ore_crops_iron_stage_4.png"},
	inventory_image = "mcl_ore_crops_iron_stage_4.png",
	wield_image = "mcl_ore_crops_iron_stage_4.png",
	drop = {
		max_items = 4,
		items = {
			{ items = {"mcl_core:iron_nugget"}, rarity = 5},
			{ items = {"mcl_core:iron_nugget"}, rarity = 5},
			{ items = {"mcl_core:iron_nugget"}, rarity = 5},
			{ items = {"mcl_core:iron_ingot"}, rarity = 10},
			{ items = {"mcl_core:iron_ingot"}, rarity = 50},
			{ items = {"mcl_core:iron_ingot"} }
		}
	},
	groups = {dig_immediate=3, not_in_creative_inventory=1, plant=1, attached_node=1,
		dig_by_water=1,destroy_by_lava_flow=1, dig_by_piston=1},
	sounds = mcl_sounds.node_sound_leaves_defaults(),
	_mcl_blast_resistance = 0,
})

mcl_farming:add_plant("plant_iron", "mcl_ore_crops:iron", {"mcl_ore_crops:iron_1", "mcl_ore_crops:iron_2", "mcl_ore_crops:iron_3", "mcl_ore_crops:iron_4", "mcl_ore_crops:iron_5", "mcl_ore_crops:iron_6", "mcl_ore_crops:iron_7"}, 25, 20)


local mod_screwdriver = minetest.get_modpath("screwdriver")
local on_rotate
if mod_screwdriver then
	on_rotate = screwdriver.rotate_3way
end